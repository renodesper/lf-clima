# Clima ☁

This project serves as a learning process to learn Flutter. It was created based on Udemy course.

Reference: https://www.udemy.com/course/flutter-bootcamp-with-dart

## What you will learn

- How to use Dart to perform asynchronous tasks.
- Understand async and await.
- Learn about Futures and how to work with them.
- How to network with the Dart http package.
- What APIs are and how to use them to get data from the internet.
- What JSONs are and how to parse them using the Dart convert package.
- How to pass data forwards and backwards between screens using the Navigator.
- How to handle exceptions in Dart using try/catch/throw.
- Learn about the lifecycle of Stateful Widgets and how to override them.
- How to use the Geolocator package to get live location data for both iOS and Android.
- How to use the TextField Widget to take user input.

## Screenshot

![Clima 1](docs/screenshot1.jpg)

![Clima 2](docs/screenshot2.jpg)
